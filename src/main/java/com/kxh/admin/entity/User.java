package com.kxh.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class User {
    private Integer id;
    private String username;
    @JsonIgnore // 接口里不返回该字段
    private String password;
    private String nickname;
    private String address;
    private String phone;
    private String email;
}