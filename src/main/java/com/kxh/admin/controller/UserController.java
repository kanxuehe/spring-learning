package com.kxh.admin.controller;

import com.kxh.admin.entity.User;
import com.kxh.admin.mapper.UserMapper;
import com.kxh.admin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;

    // 查询
    @GetMapping("/list")
    public Map<String, Object> list(@RequestParam Integer pageNo, @RequestParam Integer pageSize, @RequestParam String username){
        pageNo = (pageNo - 1) * pageSize;
        Integer total = userMapper.getUserTotal(username);
        List<User> list = userMapper.getList(pageNo, pageSize, username);
        Map<String, Object> res = new HashMap<>();
        res.put("list", list);
        res.put("count", total);
        return res;
    }
    // 新增
    @PostMapping("/create")
    public Integer save(@RequestBody User user){
        return userService.saveUser(user);
    }
    // 更新
    @PutMapping("/update")
    public Integer update(@RequestBody User user){
        return userService.saveUser(user);
    }
    // 删除
    @DeleteMapping("/delete/{id}")
    public  Integer delete(@PathVariable Integer id){
        return userMapper.deleteUserById(id);
    }
}
