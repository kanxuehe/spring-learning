package com.kxh.admin.mapper;

import com.kxh.admin.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface UserMapper {
    @Select("select * from sys_user where username like concat('%', #{username}, '%') limit #{pageNo}, #{pageSize}")
    List<User> getList(Integer pageNo, Integer pageSize, String username);

    @Select("select count(*) from sys_user where username like concat('%', #{username}, '%')")
    int getUserTotal(String username);
    @Insert("insert into sys_user(username,nickname,password,email,phone,address) values (#{username},#{nickname},#{password},#{email},#{phone},#{address})")
    int insert(User user);
    // 复杂的sql拿到xml里去写，这里就要去掉注解
    int update(User user);
    @Delete("delete from sys_user where id = #{id}")
    int deleteUserById(@Param("id") Integer id);
}
