package com.kxh.admin.service;

import com.kxh.admin.entity.User;
import com.kxh.admin.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    //  通过autowired 引入UserMapper
    @Autowired
    private UserMapper userMapper;

    public int saveUser(User user){
        if(user.getId()==null) {
            return userMapper.insert(user);
        }else {
            return userMapper.update(user);
        }
    }
}
